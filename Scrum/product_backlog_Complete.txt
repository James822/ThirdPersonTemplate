PRODUCT BACKLOG - complete:

==================================================================================

TASK NAME: Add Third Person Character

DESCRIPTION:
    - add a simple third person character with animations that can move around

USER: James822

STATUS: Complete

TIME STARTED: 2020/04/25-14:48 TIME FINISHED: 2020/04/25-15:34

==================================================================================
==================================================================================

TASK NAME: Refine Character Animation

DESCRIPTION:
    - add a sprint animation to character and smooth animation
    transtions somehow

USER: James822

STATUS: Complete

TIME STARTED: 2020/04/25-18:25 TIME FINISHED: 2020/04/25-20:31

==================================================================================
==================================================================================

TASK NAME: LERP and SLERP proficency

DESCRIPTION:
    - Make third person character rotation smooth and become 
	proficient with SLERP and Quaternion rotation

USER: King2B

STATUS: Complete

TIME STARTED: 2020/04/25-12:48 TIME FINISHED: 2020/04/27-14:55

==================================================================================
==================================================================================

TASK NAME: Camera Control changes

DESCRIPTION:
    - Adding the ability to scroll in and out on the character with the mouse wheel,
	and optional click-and-drag type camera rotation

USER: King2B

STATUS: Complete

TIME STARTED: 2020/04/27-3:21 TIME FINISHED: 2020/04/27-22:16

==================================================================================

==================================================================================

TASK NAME: Simple Enemy

DESCRIPTION:
    - Simple Enemies without navigation

USER: King2B

STATUS: Complete

TIME STARTED: 2020/04/28 10:00 TIME FINISHED: 2020/04/28 14:24

==================================================================================
==================================================================================

TASK NAME: Create Abstract Code

DESCRIPTION:
    - make some base classes for enemy and items that can be picked
    up.

USER: James822

STATUS: Complete

TIME STARTED: 2020/04/28-14:08 TIME FINISHED: 2020/04/28-15:59

==================================================================================
==================================================================================

TASK NAME: Improve Sky lighting

DESCRIPTION:
    - setup a good lighting setup with sky

USER: James822

STATUS: Complete

TIME STARTED: 2020/04/28-16:08 TIME FINISHED: 2020/04/28-17:40

==================================================================================
==================================================================================

TASK NAME: Raycasting Fundamentals

DESCRIPTION:
    - Learn raycasting fundamentals and apply them to simple enemy navigation

USER: King2B

STATUS: Complete

TIME STARTED: 2020/04/28-15:43 TIME FINISHED: 2020/04/29-8:58

==================================================================================
==================================================================================

TASK NAME: Simple Enemy Navigation

DESCRIPTION:
    - Enemy moves only to where it last saw the player

USER: King2B

STATUS: Complete

TIME STARTED: 2020/04/28-15:43 TIME FINISHED: 2020/04/29-8:58

==================================================================================
==================================================================================

TASK NAME: Refine Character Animation 2

DESCRIPTION:
    - add better walk and run animations, figure out how to do nearest key frame
    interpolation instead of linear, use AnimationTree node to blend between
    animations properly.

USER: James

STATUS: Complete

TIME STARTED: 2020/04/27-17:20 TIME FINISHED: 2020/05/01-21:35

==================================================================================
==================================================================================

TASK NAME: Refactor code in Player script

DESCRIPTION:
    - code in player script can and should be refactored into
    more scripts within the player scene

USER: James

STATUS: Complete

TIME STARTED: 2020/04/27-23:46 TIME FINISHED: 2020/05/02-18:28

==================================================================================
==================================================================================

TASK NAME: Fix Cam Target

DESCRIPTION:
    - camera needs to be fixed so that it orbits around the
    cam target

USER: James822

STATUS: Complete

TIME STARTED: 2020/05/02-21:30 TIME FINISHED: 2020/05/02-23:45

==================================================================================
==================================================================================

TASK NAME: Add Jumping

DESCRIPTION:
    - add jumping to player and gravity

USER: James822

STATUS: Complete

TIME STARTED: 2020/05/03-12:24 TIME FINISHED: 2020/05/03-14:06

==================================================================================
==================================================================================

TASK NAME: Simple Item Collection

DESCRIPTION:
    - Simple Collectable Object classes

USER: King2B

STATUS: Complete

TIME STARTED: 2020/04/30-9:30 TIME FINISHED: 2020/04/30-10:30

==================================================================================
==================================================================================

TASK NAME: Advanced Enemy Behaviour

DESCRIPTION:
    - Smarter Enemies that navigate with FOV, Detection Range, have
	random idle movements etc.

USER: King2B

STATUS: Complete

TIME STARTED: 2020/04/30-10:32 TIME FINISHED: 2020/05/04-2:24

==================================================================================
==================================================================================

TASK NAME: Create Floors Walls

DESCRIPTION:
    - Create some modular floor assets for the player
    to walk around, and also walls

USER: James822

STATUS: Complete

TIME STARTED: 2020/05/03-19:52 TIME FINISHED: 2020/05/10-18:55

==================================================================================
==================================================================================

TASK NAME: Test Indoor Level

DESCRIPTION:
    - make a simple test level indoors with indoor lighting

USER: James822

STATUS: Complete

TIME STARTED: 2020/05/08-18:45 TIME FINISHED: 2020/05/10-18:55

==================================================================================

