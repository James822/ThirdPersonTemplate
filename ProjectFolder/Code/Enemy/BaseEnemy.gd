#ISSUES: Enemy cannot currently start in view of player
extends KinematicBody

class_name BaseEnemy

var rng = RandomNumberGenerator.new()

var attack_distance # distance from player that the enemy stops following and attacks
var hit_damage
var speed_idle = 1
var speed_raged
# nodes
var enemy_shape

var following_player = false #if detected player
var last_seen_player

var rotSpeedValue = 0.1

var _idle_timer = null
var idle_move = false
var idle_distance_max = 1.0

var randomX
var randomZ

var view_distance = 10 #distance the enemy can detect the player
var player_distance = Vector3()

var player_origin_adj
var player_origin_grounded

var FOV_area
var player_in_FOV = false
var start_detecting = false
#var fov_deg = 180 #field of view degrees
# player variables

func _ready():
	FOV_area = $EnemyShape/FOV_area
	enemy_shape = $EnemyShape
	_idle_timer = Timer.new()
	add_child(_idle_timer)

	_idle_timer.connect("timeout", self, "_on_Timer_timeout")
	_idle_timer.set_wait_time(3.0)
	_idle_timer.set_one_shot(false) # Make sure it loops
	_idle_timer.start()
	
	
	
func _on_Timer_timeout():
	if following_player == false:
		idle_move = !idle_move
		rng.randomize()
		randomX = rng.randf_range(-idle_distance_max, idle_distance_max)
		randomZ = rng.randf_range(-idle_distance_max, idle_distance_max)

func _physics_process(delta):
	
	player_origin_adj = Vector3(PlayerManager.get_player_global_transform().origin.x,
	 PlayerManager.get_player_global_transform().origin.y + 0.3, 
	PlayerManager.get_player_global_transform().origin.z)
	
	player_origin_grounded = Vector3(PlayerManager.get_player_global_transform().origin.x,
	 0.3, PlayerManager.get_player_global_transform().origin.z)

	var space_state = get_world().direct_space_state
	var result = space_state.intersect_ray(get_global_transform().origin, 
	player_origin_adj, [self], collision_mask)
	
	if idle_move:
		_move_to_random()

	 
	if player_in_FOV:
		if result.has("collider") && result.collider.name == "Player":
			following_player = true
			last_seen_player = player_origin_grounded
			player_distance = last_seen_player - get_global_transform().origin
			
	
	if(player_distance != Vector3()):
		if abs(player_distance.x) < view_distance && abs(player_distance.y) < view_distance && abs(player_distance.z) < view_distance:
			if following_player == true:
				_move_to_player()
		else:
			following_player = false
	
	
func _move_to_random():
	
	
	var our_pos = get_global_transform().origin
	var random_pos = Vector3(our_pos.x + randomX, 0, our_pos.z + randomZ)
	var dir = random_pos - get_global_transform().origin
	
	var lerpRot = rotate_towards(dir, rotSpeedValue)
	if following_player == false:
		enemy_shape.transform = enemy_shape.transform.looking_at(lerpRot, Vector3.UP)
		move_and_slide(dir * speed_idle ,Vector3.UP)
	

func _move_to_player():
	
	var distance = last_seen_player - get_global_transform().origin
	var dir = (last_seen_player - get_global_transform().origin).normalized()
	var lerpRot = rotate_towards(dir, rotSpeedValue)
	enemy_shape.transform = enemy_shape.transform.looking_at(lerpRot, Vector3.UP)
	if(abs(distance.x) > attack_distance || abs(distance.z) > attack_distance):
		move_and_slide(dir * speed_raged ,Vector3.UP)
	
	#elif(abs(distance.x) <= attack_distance || abs(distance.z) <= attack_distance):
		#following_player = false

func rotate_towards(dir , speed):
	
	var dir_fixedHeight = Vector3(stepify (dir.x , 0.01) , 0 , stepify(dir.z , 0.01))
	var rot = -enemy_shape.transform.basis.z
	var rounded_rot = Vector3(stepify (rot.x, 0.01) + 0.01,
	 stepify (rot.y, 0.01) ,stepify (rot.z, 0.01) + 0.01)
	var final_rot = rounded_rot.linear_interpolate(dir_fixedHeight, speed)
	return final_rot

#func _move_to_position():
func on_player_enter_view(body):
		
		player_in_FOV = true
	
func on_player_exit_view(body):
	if !following_player:
		player_in_FOV = false


