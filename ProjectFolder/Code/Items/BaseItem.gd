extends Node

class_name BaseItem

var collectable = true
var despawns = false
var collected = false
var player_entered = false

func _ready():
	if collectable:
		connect('body_entered' , self , "_on_this_body_entered")
		connect('body_exited' , self , "_on_this_body_exited")


func _process(delta):
	#If player enters area_collision
	if collected:
		pass
	if player_entered:
		if Input.is_action_just_pressed("ui_accept"):
			queue_free()

func _on_this_body_entered(body):
	#if player presses e
	player_entered = true

func _on_this_body_exited(body):
	
	player_entered = false
