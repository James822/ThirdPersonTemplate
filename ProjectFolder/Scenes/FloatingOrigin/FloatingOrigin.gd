extends Spatial

# =============================================================================
#
# IMPORTANT: this script manages and does the origin shifting
# of every child/grandchild spatial node. This node MUST be the parent spatial
# in a given scene for it function correctly, structure your game so that
# this node is the parent of all other spatial nodes. (unless you want some
# things to not be affected by this)
#
# FEATURES:
# shifts all child objects so that the focus node is at the origin, this happens
# for the threshold specified. (only nodes that are children/grandchildren of
# this node will be affected, so pretty much everything should be parented to
# this node)
#
# PROPERTIES:
# focus : 
#	the scene that will be centered at the origin every shift, this should
# 	usually be a player or camera (it has to be a node that extends spatial
#	or is a spatial node)
# threshold : 
# 	the distance from which the focus scene will be reset, e.g. if the threshold 
#	is 6000, then every 6000 units the origin will be recentered on the focus 
#	scene
#
# =============================================================================

export(NodePath) var focus
export var threshold = 750 # default value is 750 units

var origin = Vector3(0, 0, 0)
var global_player_origin

var focus_node

func _ready():
	focus_node = get_node(focus)
	global_player_origin = focus_node.get_global_transform().origin

func _process(delta):
	global_player_origin = focus_node.get_global_transform().origin
	
	if origin.distance_to(global_player_origin) > threshold:
		self.transform.origin -= global_player_origin
		
