extends Area


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var enemy_parent

# Called when the node enters the scene tree for the first time.
func _ready():
	enemy_parent = get_owner()
	print(enemy_parent)
	connect('body_entered' , enemy_parent , "on_player_enter_view")
	connect('body_exited' , enemy_parent , "on_player_exit_view")
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
