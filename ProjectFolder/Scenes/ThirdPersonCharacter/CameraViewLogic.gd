extends Spatial

enum ZoomState {
	ZOOM_NONE
	ZOOM_IN
	ZOOM_OUT
}

var camera_zoom = Vector3()
var zoom = ZoomState.ZOOM_NONE

var camera_distance_multiplier = 0.3 #Scroll Strength
var camera_zoom_time = 0.2 # Scroll Time 0 - 0.99 (How long the lerp function runs) 
var camera_zoom_smooth = 0.00001 # Scroll Smoothness
var camera_zoom_max = -11 # Negative because forward 
var camera_zoom_min = -0.5
var camera_zoom_spawn = -4 # Starting distance
var camera_recenter_interpolation_time = 8 # faster as it goes up

onready var _tween_node = $Tween
var cam_target_origin
var self_global_origin

# variable set by parent (also has a default value)
var mouse_sensitivity = 0.16

# nodes
var rotation_helper_x
var camera
var cam_target


var x
var y 

func _ready():
	rotation_helper_x = $RotationHelperX
	camera = $RotationHelperX/Camera
	cam_target = get_parent().get_node("PlayerShape/CamTarget")
	camera_zoom = Vector3(0, 0, -1) * camera_zoom_spawn 
	camera.translate(camera_zoom)

func _process(delta):
	if zoom != ZoomState.ZOOM_NONE: # not mouse wheel change
		camera_zoom = camera_zoom.linear_interpolate(Vector3(0, 0, 1) * camera_distance_multiplier, 1 - camera_zoom_time)
		var camera_zoom_rounded = Vector3(stepify (camera_zoom.x, camera_zoom_smooth), stepify (camera_zoom.y, camera_zoom_smooth),stepify (camera_zoom.z, camera_zoom_smooth))
		if camera_zoom_rounded == Vector3(0, 0, 1) * camera_distance_multiplier:
			zoom = ZoomState.ZOOM_NONE
			camera_zoom = Vector3()
		#print(zoom_multiplier)
		camera.translate(camera_zoom_rounded)
		if camera.transform.origin.z <= camera_zoom_max:
			camera.transform.origin.z = camera_zoom_max
		if camera.transform.origin.z >= camera_zoom_min:
			camera.transform.origin.z = camera_zoom_min
	
	#set_camera_rotation(x , y)
	_set_camera_target(delta)

func _input(event):

	if event is InputEventMouseButton:
		if event.is_pressed():

			if event.button_index == BUTTON_WHEEL_UP:
				# zoom in
				zoom = ZoomState.ZOOM_IN
				camera_distance_multiplier = 0 - abs(camera_distance_multiplier)

			if event.button_index == BUTTON_WHEEL_DOWN:
				# zoom out
				zoom = ZoomState.ZOOM_OUT

				camera_distance_multiplier = abs(camera_distance_multiplier)
			
			
			#OPTIONAL SECTION BELOW : Click and drag camera movement (needs more sensitivity though)
				
			#if event.button_index == BUTTON_RIGHT and event.pressed:
				#right_mouse_button_pressed = true
		
		#elif not event.is_pressed():
			#if event.button_index == BUTTON_RIGHT:
				#right_mouse_button_pressed = false
				

	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:

		x = deg2rad(event.relative.y) * mouse_sensitivity
		y = -deg2rad(event.relative.x) * mouse_sensitivity
		rotation_helper_x.rotate_x(x) # Vertical Swipe
		self.rotate_y(y)	 # Horizontal Swipe
		var camera_rot = rotation_helper_x.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, 0, 70)
		rotation_helper_x.rotation_degrees = camera_rot

func set_camera_rotation(x,y):
	pass

func _set_camera_target(delta):
	"""
	this centers the camera on CamTarget, it also
	uses interpolation for a smooth result instead of it being
	janky
	"""
	self_global_origin = self.get_global_transform().origin
	cam_target_origin = cam_target.get_global_transform().origin
	
	self_global_origin = self_global_origin.linear_interpolate(cam_target_origin, 
	delta * camera_recenter_interpolation_time)
	
	var new_transform = Transform(self.transform.basis, self_global_origin)
	self.set_global_transform(new_transform)
