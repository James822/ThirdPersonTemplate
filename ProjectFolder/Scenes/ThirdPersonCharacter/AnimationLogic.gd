extends AnimationTree

"""
This class handles all the complicated animation logic
behind the scenes, the only thing you need to use this
for is to call queue_animation() on any animation that you want
to play and this script will do the heavy lifting.
"""

var transition_time = .4

onready var _tween_node = $TransitionTween
var _curr_animation

var _blend_amount
var _blend_2_amount

func queue_animation(animation):
	"""
	queues next animation to be played
	
	parameters:
		animation: string that represent the next animation
		to be queued.
	"""
	_curr_animation = animation
		
func _process(delta):
	# Blend 2 : parameters/Blend2/blend_amount
	# Blend 2 2 : parameters/Blend2 2/blend_amount
	if _curr_animation == "Idle":
		_blend_2_amount = get("parameters/Blend2 2/blend_amount")
		_tween_node.interpolate_property(self, 
		"parameters/Blend2 2/blend_amount", _blend_2_amount, 0.0, 
		transition_time, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		
		_blend_amount = get("parameters/Blend2/blend_amount")
		_tween_node.interpolate_property(self, "parameters/Blend2/blend_amount",
		_blend_amount, 0.0, transition_time, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		_tween_node.start()
		
	if _curr_animation == "Walk":
		_blend_2_amount = get("parameters/Blend2 2/blend_amount")
		_tween_node.interpolate_property(self, 
		"parameters/Blend2 2/blend_amount", _blend_2_amount, 0.0, 
		transition_time, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		
		_blend_amount = get("parameters/Blend2/blend_amount")
		_tween_node.interpolate_property(self, "parameters/Blend2/blend_amount",
		_blend_amount, 1.0, transition_time, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		_tween_node.start()
		
	if _curr_animation == "Run":
		_blend_2_amount = get("parameters/Blend2 2/blend_amount")
		_tween_node.interpolate_property(self, 
		"parameters/Blend2 2/blend_amount", _blend_2_amount, 1.0, 
		transition_time, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		_tween_node.start()
