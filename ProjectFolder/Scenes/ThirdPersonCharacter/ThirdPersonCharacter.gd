
extends KinematicBody

signal update_player_manager(player)

var dir = Vector3()
var velocity = Vector3()
var vertical_vel = Vector3()
var jump_speed = 7
var gravity = 10
var jumped = false

var move_transform_basis

var walking_speed = 1.6
var sprint_speed = 5.6

var mouse_sensitivity = 0.16

var rotSpeedValue = 0.3

# node variables here
var rotation_helper
var player_shape
var animation_controller

func _ready():
	rotation_helper = $RotationHelper
	player_shape = $PlayerShape
	animation_controller = $PlayerShape/Character/AnimationController
	
	# setting stuff in the child nodes
	rotation_helper.mouse_sensitivity = mouse_sensitivity
	
	# for PlayerManager
	self.connect("update_player_manager", PlayerManager, "_get_player")
	emit_signal("update_player_manager", self)

func _physics_process(delta):
	
	dir = Vector3()
	
	move_transform_basis = rotation_helper.transform.basis
	
	if Input.is_action_pressed("move_forward"):
		dir += move_transform_basis.z
	elif Input.is_action_pressed("move_backward"):
		dir -= move_transform_basis.z 
		
	if Input.is_action_pressed("move_right"):
		dir -= move_transform_basis.x 
	elif Input.is_action_pressed("move_left"):	
		dir += move_transform_basis.x 
	
	if Input.is_action_pressed("jump") and self.is_on_floor():
		jumped = true
	else:
		jumped = false

	var speed = walking_speed
	if dir != Vector3(): # if the player is moving
		
		var start = player_shape.transform.basis.z
		var start2 = Vector3(stepify (start.x, 0.01) + 0.01, stepify (start.y, 0.01) ,stepify (start.z, 0.01) + 0.01)
		var dir2 = Vector3(stepify (dir.x, 0.01), stepify (dir.y, 0.01),stepify (dir.z, 0.01))
		var pos = start2.linear_interpolate(dir2, rotSpeedValue)
		player_shape.transform = player_shape.transform.looking_at(pos, Vector3.UP)
		player_shape.transform.basis.z = -player_shape.transform.basis.z
		if Input.is_action_pressed("sprint"):
			animation_controller.queue_animation("Run")
			rotSpeedValue = 0.13
			speed = sprint_speed
		
		else:
			animation_controller.queue_animation("Walk")
			rotSpeedValue = 0.1
	else:
		animation_controller.queue_animation("Idle")
		
	dir = dir.normalized()
	if not self.is_on_floor():
		vertical_vel.y -= gravity * delta
	else:
		if jumped:
			vertical_vel.y = jump_speed
		else:
			vertical_vel.y = 0
	velocity = speed * dir
	velocity.y = vertical_vel.y
	move_and_slide(velocity, Vector3.UP)
