extends Node

"""
This autoload class manages player information and
can be accessed globally by any object.

The class contains many read-only variables about
player data like speed, direction, and many other
things.
"""

var _player

# signal reciever function that get's a reference to the player.
func _get_player(player):
	_player = player

func get_player_global_transform():
	return _player.get_global_transform()
